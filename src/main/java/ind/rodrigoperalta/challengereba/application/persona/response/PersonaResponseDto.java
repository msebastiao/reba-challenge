package ind.rodrigoperalta.challengereba.application.persona.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import ind.rodrigoperalta.challengereba.domain.persona.model.Persona;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor(force = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PersonaResponseDto {

    private final String numeroDocumento;

    private final String tipoDocumento;

    private final String id;

    private final String pais;

    private final List<String> emails;

    private final List<String> telefonos;

    public static PersonaResponseDto fromDomain(final Persona persona) {


        return new PersonaResponseDto(
            persona.getDocumento().getNumeroDocumento(),
            persona.getDocumento().getTipoDocumento().toString(),
            persona.getResourceId(),
            persona.getPais(),
            new ArrayList<>(persona.getContacto().getEmails()),
            new ArrayList<>(persona.getContacto().getPhones())
        );

    }

}
