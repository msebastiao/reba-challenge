package ind.rodrigoperalta.challengereba.application.persona.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PersonaRequestDto {

    private String tipoDocumento;

    private String numeroDocumento;

    private String pais;

    private String fechaNacimiento;

    private List<String> emails;

    private List<String> telefonos;

}
