package ind.rodrigoperalta.challengereba.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.text.MessageFormat;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidDomainException extends RuntimeException {

    public InvalidDomainException(String message, Object... args) {
        super(MessageFormat.format(message, args));
    }

}
