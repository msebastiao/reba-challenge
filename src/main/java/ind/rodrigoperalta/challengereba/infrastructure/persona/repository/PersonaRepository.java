package ind.rodrigoperalta.challengereba.infrastructure.persona.repository;

import ind.rodrigoperalta.challengereba.infrastructure.persona.model.PersonaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PersonaRepository extends JpaRepository<PersonaEntity, String> {

    boolean existsById_TipoDocumentoAndId_NumeroDocumentoAndId_Pais(final String tipoDocumento, final String numeroDocumento, final String pais);

    boolean existsByResourceId(final String resourceId);

    Optional<PersonaEntity> findByResourceId(final String resourceId);

    void deleteByResourceId(final String resourceId);

}
