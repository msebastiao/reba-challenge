package ind.rodrigoperalta.challengereba.infrastructure.persona.model;

import ind.rodrigoperalta.challengereba.domain.persona.model.Contacto;
import ind.rodrigoperalta.challengereba.domain.persona.model.Documento;
import ind.rodrigoperalta.challengereba.domain.persona.model.Persona;
import ind.rodrigoperalta.challengereba.domain.persona.model.TipoDocumento;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class PersonaEntity {

    @EmbeddedId
    private PersonaEntityId id;

    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @ElementCollection
    private List<String> emails;

    @ElementCollection
    private List<String> phones;

    @Column(name = "resource_id", unique = true)
    private String resourceId;

    public static PersonaEntity fromDomain(final Persona persona) {

        final var contacto = persona.getContacto();

        return new PersonaEntity(
            PersonaEntityId.fromDomain(persona),
            persona.getFechaNacimiento(),
            contacto.getEmails(),
            contacto.getPhones(),
            persona.getResourceId()
        );

    }

    public Persona toDomain() {

        final var numeroDocumento = this.id.getNumeroDocumento();
        final var tipoDocumento = TipoDocumento.valueOf(this.id.getTipoDocumento());
        final var pais = this.id.getPais();

        return Persona.createWithId(
            this.resourceId,
            Documento.create(
                tipoDocumento, numeroDocumento
            ),
            pais,
            this.getDateOfBirth(),
            Contacto.createContacto(
                Collections.unmodifiableList(this.emails),
                Collections.unmodifiableList(this.phones)
            )
        );

    }

}
