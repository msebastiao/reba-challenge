package ind.rodrigoperalta.challengereba.domain.persona.actions;

import ind.rodrigoperalta.challengereba.domain.persona.model.Persona;
import ind.rodrigoperalta.challengereba.infrastructure.persona.PersistenciaPersonaService;
import org.springframework.stereotype.Component;

@Component
public class PersistPersonaAction {

    private final PersistenciaPersonaService persistenciaPersonaService;

    public PersistPersonaAction(final PersistenciaPersonaService persistenciaPersonaService) {
        this.persistenciaPersonaService = persistenciaPersonaService;
    }

    public void execute(final Persona persona) {

        persistenciaPersonaService.validarPersonaExistente(persona);

        persistenciaPersonaService.persistirPersona(persona);

    }

}
