package ind.rodrigoperalta.challengereba.domain.persona.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Value;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import java.util.UUID;

@Value
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Persona {

    public static final int MINIMUM_YEARS_OF_PERSON = 18;

    String resourceId;

    Documento documento;

    String pais;

    LocalDate fechaNacimiento;

    Contacto contacto;

    public static Persona create(
        final Documento documento,
        final String pais,
        final LocalDate fechaNacimiento,
        final Contacto contacto
    ) {

        validarParametros(documento, pais, fechaNacimiento, contacto);
        validarMayorDeEdad(fechaNacimiento);

        return new Persona(
            UUID.randomUUID().toString(),
            documento,
            pais,
            fechaNacimiento,
            contacto
        );

    }

    private static void validarParametros(
        final Documento documento, final String pais,
        final LocalDate fechaNacimiento, final Contacto contacto) {

        if (Objects.isNull(documento)) {
            // TODO change to a custom exception wtih 400
            throw new RuntimeException("Documento must not be null");
        }

        if (Objects.isNull(pais) || pais.isBlank()) {
            // TODO change to a custom exception wtih 400
            throw new RuntimeException("Pais must not be null or blank");
        }

        if (Objects.isNull(fechaNacimiento)) {
            // TODO change to a custom exception wtih 400
            throw new RuntimeException("Fecha nacimiento must not be null");
        }

        if (Objects.isNull(contacto)) {
            // TODO change to a custom exception wtih 400
            throw new RuntimeException("Contacto must not be null");
        }
    }

    private static void validarMayorDeEdad(final LocalDate fechaNacimiento) {

        final var now = LocalDate.now();
        final var yearDifference = ChronoUnit.YEARS.between(fechaNacimiento, now);

        if (yearDifference < MINIMUM_YEARS_OF_PERSON) {
            throw new RuntimeException("Person must be older than 18 years old");
        }

    }

    public static Persona createWithId(
        final String withId,
        final Documento documento,
        final String pais,
        final LocalDate fechaNacimiento,
        final Contacto contacto) {

        validarParametros(documento, pais, fechaNacimiento, contacto);
        validarMayorDeEdad(fechaNacimiento);

        return new Persona(withId, documento, pais, fechaNacimiento, contacto);

    }

    public Persona updateBasicData(
        final Documento documento,
        final String pais,
        final LocalDate fechaNacimiento,
        final Contacto contacto) {

        return new Persona(this.getResourceId(), documento, pais, fechaNacimiento, contacto);

    }
}

