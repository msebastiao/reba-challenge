package ind.rodrigoperalta.challengereba.domain.persona.actions;

import ind.rodrigoperalta.challengereba.domain.persona.model.Contacto;
import ind.rodrigoperalta.challengereba.domain.persona.model.Documento;
import ind.rodrigoperalta.challengereba.domain.persona.model.Persona;
import ind.rodrigoperalta.challengereba.infrastructure.persona.PersistenciaPersonaService;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class UpdatePersonaAction {

    private final PersistenciaPersonaService persistenciaPersonaService;

    public UpdatePersonaAction(final PersistenciaPersonaService persistenciaPersonaService) {

        this.persistenciaPersonaService = persistenciaPersonaService;

    }

    public Persona execute(
        final String resourceId,
        final Documento documento,
        final String pais,
        final LocalDate fechaNacimiento,
        final Contacto contacto) {

        final var persona = persistenciaPersonaService.obtenerPersonaPorResourceId(resourceId);

        final var updatedPersona = persona.updateBasicData(
            documento, pais, fechaNacimiento, contacto
        );

        this.persistenciaPersonaService.persistirPersona(updatedPersona);

        return updatedPersona;

    }
}
