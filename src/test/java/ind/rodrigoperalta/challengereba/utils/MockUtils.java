package ind.rodrigoperalta.challengereba.utils;

import ind.rodrigoperalta.challengereba.application.persona.request.PersonaRequestDto;
import ind.rodrigoperalta.challengereba.domain.persona.model.Contacto;
import ind.rodrigoperalta.challengereba.domain.persona.model.Documento;
import ind.rodrigoperalta.challengereba.domain.persona.model.Persona;
import ind.rodrigoperalta.challengereba.domain.persona.model.TipoDocumento;
import ind.rodrigoperalta.challengereba.infrastructure.persona.model.PersonaEntity;
import ind.rodrigoperalta.challengereba.infrastructure.persona.model.PersonaEntityId;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

public class MockUtils {

    public static final String tipoDocumento = "DNI";
    public static final String numeroDocumento = "123";
    public static final String pais = "Argentina";
    public static final String fechaNacimiento = "1996-05-09";
    public static final LocalDate fechaNacimientoDate = LocalDate.of(1996, 5, 9);
    public static final String email = "a@a.com";
    public static final String phone = "1167913110";

    public static Documento givenValidDocumento() {
        return Documento.create(TipoDocumento.DNI, numeroDocumento);
    }

    public static Contacto givenValidContacto() {
        return Contacto.createContacto(
            List.of(email), List.of(phone)
        );
    }

    public static PersonaRequestDto givenValidPersonaRequestDto() {

        return new PersonaRequestDto(
            tipoDocumento,
            numeroDocumento,
            pais,
            fechaNacimiento,
            Collections.singletonList(email),
            Collections.singletonList(phone)
        );

    }

    public static PersonaRequestDto givenValidPersonaRequestDtoForUpdate() {

        return new PersonaRequestDto(
            "PASAPORTE",
            "AA44548",
            "Chile",
            "1991-09-04",
            Collections.emptyList(),
            Collections.singletonList("123456789")
        );

    }

    public static Persona givenValidPersona() {

        return Persona.create(
            givenValidDocumento(),
            "Argentina",
            LocalDate.parse(fechaNacimiento),
            givenValidContacto()
        );

    }

    public static Persona givenValidPersonaWithId(
        final String withId
    ) {

        return Persona.createWithId(
            withId,
            givenValidDocumento(),
            "Argentina",
            LocalDate.parse(fechaNacimiento),
            givenValidContacto()
        );

    }

    public static PersonaEntity givenValidPersonaEntityWithResourceId(final String resourceId) {

        final var personaEntityId = new PersonaEntityId(
            tipoDocumento, numeroDocumento, pais
        );

        return new PersonaEntity(
            personaEntityId,
            fechaNacimientoDate,
            Collections.singletonList(email),
            Collections.singletonList(phone),
            resourceId
        );


    }
}
