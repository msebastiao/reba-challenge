package ind.rodrigoperalta.challengereba.domain.persona.actions;

import ind.rodrigoperalta.challengereba.infrastructure.persona.PersistenciaPersonaService;
import ind.rodrigoperalta.challengereba.utils.MockUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

class GetPersonaActionTest {

    private final PersistenciaPersonaService persistenciaPersonaService;
    private final GetPersonaAction getPersonaAction;

    public GetPersonaActionTest() {

        this.persistenciaPersonaService = mock(PersistenciaPersonaService.class);
        this.getPersonaAction = new GetPersonaAction(
            this.persistenciaPersonaService
        );
    }

    @Nested
    @DisplayName("Given a valid resource id of a persisted person")
    class GivenValidResourceIdOfAPersistedPerson {

        @Nested
        @DisplayName("When executing action")
        class WhenExecutingAction {

            @Test
            @DisplayName("Then should return person")
            void thenShouldReturnPerson() {

                // given
                final var id = UUID.randomUUID().toString();

                given(persistenciaPersonaService.obtenerPersonaPorResourceId(id))
                    .willReturn(MockUtils.givenValidPersonaWithId(id));

                // when
                final var persona = getPersonaAction.execute(id);

                // then
                assertThat(persona)
                    .isNotNull();

                assertThat(persona.getResourceId())
                    .isNotNull()
                    .isEqualTo(id);

            }

        }

    }

}
