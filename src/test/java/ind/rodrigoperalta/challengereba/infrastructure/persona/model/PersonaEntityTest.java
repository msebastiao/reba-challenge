package ind.rodrigoperalta.challengereba.infrastructure.persona.model;

import ind.rodrigoperalta.challengereba.utils.MockUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PersonaEntityTest {

    @Nested
    @DisplayName("Given valid persona with one phone and one email")
    class GivenValidPersonaWithOnePhoneAndOneEmail {

        @Nested
        @DisplayName("When creating persona entity")
        class WhenCreatingPersonaEntity {

            @Test
            @DisplayName("Then should return persona entity")
            void thenShouldReturnPersonaEntity() {

                // given
                final var persona = MockUtils.givenValidPersona();

                // when
                final var entity = PersonaEntity.fromDomain(persona);

                // then
                assertThat(entity)
                    .isNotNull();

                assertThat(entity.getId())
                    .isNotNull();

                assertThat(entity.getResourceId())
                    .isNotBlank();

                assertThat(entity.getDateOfBirth())
                    .isNotNull();

                assertThat(entity.getEmails())
                    .isNotNull()
                    .isNotEmpty()
                    .hasSize(1);

                assertThat(entity.getPhones())
                    .isNotNull()
                    .isNotEmpty()
                    .hasSize(1);

            }

        }

    }

}
