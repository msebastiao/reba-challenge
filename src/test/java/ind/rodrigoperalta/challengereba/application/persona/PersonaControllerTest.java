package ind.rodrigoperalta.challengereba.application.persona;

import ind.rodrigoperalta.challengereba.domain.persona.actions.CreatePersonaAction;
import ind.rodrigoperalta.challengereba.domain.persona.actions.GetPersonaAction;
import ind.rodrigoperalta.challengereba.domain.persona.actions.UpdatePersonaAction;
import ind.rodrigoperalta.challengereba.domain.persona.model.Contacto;
import ind.rodrigoperalta.challengereba.domain.persona.model.Documento;
import ind.rodrigoperalta.challengereba.utils.MockUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;

import java.time.LocalDate;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

class PersonaControllerTest {

    private final PersonaController personaController;

    private final CreatePersonaAction createPersonaAction;

    private final UpdatePersonaAction updatePersonaAction;

    private final GetPersonaAction getPersonaAction;

    public PersonaControllerTest() {

        this.createPersonaAction = Mockito.mock(CreatePersonaAction.class);

        this.updatePersonaAction = Mockito.mock(UpdatePersonaAction.class);

        this.getPersonaAction = Mockito.mock(GetPersonaAction.class);

        this.personaController = new PersonaController(
            this.createPersonaAction, this.updatePersonaAction, this.getPersonaAction
        );

    }

    @Nested
    @DisplayName("Given a valid request without id")
    class GivenAValidPersonaRequestDtoWithoutId {

        @Nested
        @DisplayName("When executing crearPersona")
        class WhenExecutingCrearPersona {

            @Test
            @DisplayName("Then should return response entity")
            void thenShouldReturnResponseEntity() {

                // given
                final var dto = MockUtils.givenValidPersonaRequestDto();

                when(createPersonaAction.execute(any(), any(), any(), any()))
                    .thenReturn(MockUtils.givenValidPersona());

                // when
                final var responseEntity = personaController.crearPersona(dto);

                // then
                Assertions.assertThat(responseEntity.getStatusCode())
                    .isEqualTo(HttpStatus.OK);

                Mockito.verify(createPersonaAction, times(1))
                    .execute(any(Documento.class), anyString(), any(LocalDate.class), any(Contacto.class));

            }

        }

    }


    @Nested
    @DisplayName("Given a valid request with id")
    class GivenAValidPersonaRequestDtoWithId {

        @Nested
        @DisplayName("When executing modificarPersona")
        class WhenExecutingModificarPersona {

            @Test
            @DisplayName("Then should return response")
            void thenShouldReturnResponseEntity() {

                // given
                final var dto = MockUtils.givenValidPersonaRequestDto();
                final var id = UUID.randomUUID().toString();

                when(updatePersonaAction.execute(anyString(), any(Documento.class), anyString(), any(LocalDate.class), any(Contacto.class)))
                    .thenReturn(MockUtils.givenValidPersonaWithId(id));

                // when
                final var responseEntity = personaController.modificarPersona(id, dto);

                // then
                Assertions.assertThat(responseEntity.getStatusCode())
                    .isEqualTo(HttpStatus.OK);

                Mockito.verify(updatePersonaAction, times(1))
                    .execute(anyString(), any(Documento.class), anyString(), any(LocalDate.class), any(Contacto.class));

            }

        }

    }

    @Nested
    @DisplayName("Given a valid persisted persona id")
    class GivenAValidPersonaResourceId {

        @Nested
        @DisplayName("When getting persona")
        class WhenGettingPersona {

            @Test
            @DisplayName("Then should return persona response dto")
            void thenShouldReturnPersonaResponseDto() {

                // given
                final var id = UUID.randomUUID().toString();

                // when
                when(getPersonaAction.execute(anyString()))
                    .thenReturn(MockUtils.givenValidPersonaWithId(id));

                // then
                final var responseEntity = personaController.obtenerPersona(id);

                // then
                Assertions.assertThat(responseEntity.getStatusCode())
                    .isEqualTo(HttpStatus.OK);

                Mockito.verify(getPersonaAction, times(1))
                    .execute(id);

            }

        }

    }


    // TODO Unhappy paths: persona not found when updating, invalid persona to create/update, persona not found on GET
    // (most of these cases are already covered in the other layers though)
}
